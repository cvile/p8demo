app.directive("p8CreateForm", function($http, $resource) {
      return {
        restrict : 'E',
        scope : true,
        link : function(scope, elem, attr){
            var link = attr.httpLink;
            scope.form = {
                code : "",
                city : "",
                language : "",
                name : "",
                population : "",
                river : "",
                somethingcool : ""
            }
            scope.save = function(){
                var xsrf = $.param(scope.form);
                $http({
                    method: 'POST',
                    url: link,
                    data: xsrf,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                }).success(function(data){
                    console.log(data);
                    alert("wooo hooo, you successfuly created data");
                }).error(function(err){
                    console.log(err);
                    alert("an error occured");
                });
            }

            scope.cancel = function(){
                scope.form = {
                    code : "",
                    name : "",
                    population : "",
                    language : "",
                    city : "",
                    river :"",
                    somethingcool :""
                };
            }
        },
        templateUrl : 'views/partials/formDirectiveView.html'
    };
});
