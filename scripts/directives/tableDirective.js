angular.module('p8demoApp').directive("p8Table", function($http) {
    return {
        restrict : 'E',
        scope : true,
        link : function(scope, elem , attr){
            var isEditable = attr.editable;
            var httpLink = attr.httpLink;

            $http.get(httpLink).success(function(data){
                scope.stefan = data;
                console.log(data);
            });

            scope.addNew = function(){
                var temp = {
                    city : "",
                    code : "",
                    language : "",
                    population : "",
                    river : "",
                    somethingCool : ""
                }
                scope.stefan.push(temp);
            }

            scope.save = function(){
                console.log(scope.stefan);
                // $http.post(httpLink, scope.stefan).success(function(data){
                //     alert("you successfuly updated table");
                // }).error(function(err){
                //     alert("an error occured");
                // });
            }

            scope.delete = function(_id){
                var link = 'http://stefanvukovic.me/p8/yii/api/web/v1/countries';
                $http.delete(link+'/'+_id).success(function(data){
                    for(var i=0; i < scope.stefan.length; i++){
                        if(scope.stefan[i].code === _id){
                            scope.stefan.splice(i, 1);
                        }   
                    }
                    alert("you successfuly deleted item");
                }).error(function(data){
                    alert("an error occured");
                });
            }
        },
        templateUrl : 'views/partials/tableDirectiveView.html'
    };
});

