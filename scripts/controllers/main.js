'use strict';

/**
 * @ngdoc function
 * @name p8demoApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the p8demoApp
 */
angular.module('p8demoApp')

  .controller('MainCtrl', function ($scope, $http) {
    $scope.editable = false;
    $scope.editDisabled = false;
    $scope.saveDisabled = true;
    $scope.users = [
      {id: 1, name: 'awesome user1', status: 2, group: 4, groupName: 'admin'},
      {id: 2, name: 'awesome user2', status: undefined, group: 3, groupName: 'vip'},
      {id: 3, name: 'awesome user3', status: 2, group: null, groupName : "system"},
      {id: 1, name: 'awesome user1', status: 2, group: 4, groupName: 'admin'},
      {id: 2, name: 'awesome user2', status: undefined, group: 3, groupName: 'vip'},
      {id: 3, name: 'awesome user3', status: 2, group: null, groupName : "system"}
    ];

    $scope.editTable = function(){
      $scope.editable = true;
      $scope.editDisabled = true;
      $scope.saveDisabled = false;
    }

    $scope.saveTable = function(){
      $scope.editable = false;
      $scope.saveDisabled = true;
      $scope.editDisabled = false;
      console.log($scope.users);
    }

    $scope.addNewRow = function(){
      var temp = {
        id : "",
        name : "",
        status : "",
        group : "",
        groupName : ""
      };
      $scope.users.push(temp);
    }

    $scope.deleteSelectedRow = function(_index){
      $scope.users.splice(_index, 1);
    }

    // $http.get('http://stefanvukovic.me/p8/yii/api/web/v1/countries').success(function(data){
    //   console.log(data);
    // });
  });
