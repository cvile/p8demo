'use strict';

/**
 * @ngdoc overview
 * @name p8demoApp
 * @description
 * # p8demoApp
 *
 * Main module of the application.
 */
var app = angular
  .module('p8demoApp', [
    'ngRoute',
    'ngResource'
  ])
  .config(function ($routeProvider, $httpProvider, $resourceProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/create', {
        templateUrl: 'views/create.html',
        controller: 'CreateController'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
